<?php

return array(
    
        'doctrine' => array(
        'driver' => array(
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'main_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                   __DIR__ . '/../src/Main/Entity', 
                ),
            ),

            // default metadata driver, aggregates all other drivers into a single one.
            // Override `orm_default` only if you know what you're doing
            'orm_default' => array(
                'drivers' => array(
                    // register `my_annotation_driver` for any entity under namespace `My\Namespace`
                    'Main\Entity' => 'main_entity'
                )
            )
        )
    ),
       
    'controllers'=> array(
        'invokables' => array (
            'Main\Controller\Auth' => 'Main\Controller\AuthController',
            'Main\Controller\Index' => 'Main\Controller\IndexController',
            'Main\Controller\Reg' => 'Main\Controller\RegController',
            'Main\Controller\Start' => 'Main\Controller\StartController',
        ),
    ),
    'router' => array(
        'routes'=> array(
            'login'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/auth/[:action/]',
                    'defaults'=> array(
                        '__NAMESPACE__' => 'Main\Controller',
                        'controller'=>'Auth',
                        'action' => 'login',
                    ),
                ),
            ),
            'index'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/index/[:action/]',
                    'defaults'=> array(
                        '__NAMESPACE__' => 'Main\Controller',
                        'controller'=>'Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'reg'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/reg/[:action/]',
                    'defaults'=> array(
                        '__NAMESPACE__' => 'Main\Controller',
                        'controller'=>'Reg',
                        'action' => 'index',
                    ),
                ),
            ),           
             'start'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/start/[:action/]',
                    'defaults'=> array(
                        '__NAMESPACE__' => 'Main\Controller',
                        'controller'=>'Start',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager'=> array(
        'template_path_stack'=> array(
            __DIR__ . '/../view',
        ),
    ),
);