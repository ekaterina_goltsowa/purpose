<?php
namespace Main\Filter;

use Zend\InputFilter\InputFilter;

class LoginFormInputFilter extends InputFilter
{
    public function __construct() {
        $this->add(array(
            'name' => 'login',
            'required'=>true,
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'min'=>3,
                        'max'=>25,
                    ),                
                ),
            ),
            'filters'=> array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),               
            ),
        ));
        
        $this->add(array(
            'name' => 'password',
            'required'=>true,
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'min'=>6,
                        'max'=>20,
                    ),                
                ),
            ),
            'filters'=> array(
                array('name' => 'StringTrim'),       
                array('name' => 'StripTags'),
            ),
        ));
                        
    }
}
