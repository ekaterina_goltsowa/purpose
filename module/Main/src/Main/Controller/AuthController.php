<?php
namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Annotation\AnnotationBuilder;

use Main\Entity\User;
use Main\Form\LoginForm;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage;

class AuthController extends AbstractActionController
{
    protected $form;
    protected $authservice;
    
    public function getLogin()
    {
        return $this->getServiceLocator()->get('AuthService')->getStorage()->read();
    }
    
    public function getIdByLogin()
    {
        $login = $this->getLogin();
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT u.id FROM Main\Entity\User u WHERE u.login='$login'");
        $rows = $query->getResult();
        $id=$rows[0]['id'];
        return $id;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
    public function getEntityManager()
    {
       return $entityManager=$this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }  
    

    public function getForm()
    {
        if (! $this->form) {
            $user       = new User();
            $builder    = new AnnotationBuilder();
            $this->form = $builder->createForm($user);
        }
         
        return $this->form;
    }
     
    public function loginAction()
    {
        //if already login, redirect to success page 
        if ($this->getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('index');
        }
                 
        $form  = $this->getForm();
        return array(
            'form'      => $form,
            'messages'  => $this->flashmessenger()->getMessages()
        );
    }
     
    public function authenticateAction()
    {
       $status=$message='';
       $form = $this->getForm();
        $redirect = 'login';
         
        $request = $this->getRequest();
  
      
        if ($request->isPost()){
            $form->setData($request->getPost());
            
           // if ($form->isValid()){
                $this->getAuthService()->getAdapter()
                                       ->setIdentity($request->getPost('login'))
                                       ->setCredential($request->getPost('password'));
                                        
                $result = $this->getAuthService()->authenticate();
                  
                 
                if ($result->isValid()) {
                    $redirect = 'index';
                    $this->getAuthService()->getStorage()->write($request->getPost('login'));
                }
                else 
                {
                    $message= 'Неверный логин или пароль!';
                    $status = 'error';
                     $this->flashMessenger()
                             ->setNamespace($status)
                             ->addMessage($message);
                }
           // }
        }
         
        return $this->redirect()->toRoute($redirect); 
    }
     
    public function logoutAction()
    {
        $this->getAuthService()->clearIdentity();
         
        $this->flashmessenger()->addMessage("Алоха!");
        return $this->redirect()->toRoute('login');
    }
    
    public function deleteAccountAction()
    {
       $em= $this->getEntityManager();
       $repository = $em->getRepository('Main\Entity\User');
       $user = $repository->find($this->getIdByLogin());
       $em->remove($user);
       $em->flush();  
       
        $this->getAuthService()->clearIdentity();
        $this->flashmessenger()->addMessage("Прощайте! Нам будет вас нехватать :(");
        return $this->redirect()->toRoute('login');
    }
}



