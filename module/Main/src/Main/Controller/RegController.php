<?php 
namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Form\Annotation\AnnotationBuilder;
use Main\Entity\Category;
use Main\Entity\User;
use Main\Entity\Purpose;

class RegController extends AbstractActionController
{
    protected $form;
    protected $authservice;
    
    public function getEntityManager()
    {
       return $entityManager=$this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
    
    public function getForm()
    {
        if (! $this->form) {
           $user  = new User();
            $builder    = new AnnotationBuilder();
            $this->form = $builder->createForm($user);
        }
         
        return $this->form;
    }
    
    public function getIdByLogin($login)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT u.id FROM Main\Entity\User u WHERE u.login='$login'");
        $rows = $query->getResult();
        $id=$rows[0]['id'];
        return $id;
    }
           
    public function indexAction() {
        if ($this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('index');
        }      
        $form  = $this->getForm();
        return array('form'=> $form);
    }
    
    public function registrationAction()
    {
        $em = $this->getEntityManager();
        $status=$message='';
        $user  = new User();
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($user);
        $request = $this->getRequest();   
        if ($request->isPost()){
            $form->setData($request->getPost());
            if ($form->isValid()){ 
                $login = $request->getPost('login');
                $query = $em->createQuery("SELECT u.id FROM Main\Entity\User u WHERE u.login='$login'");
                $rows = $query->getResult();
                if (count($rows)>0){
                    $message= 'Пользователь с данным логином уже существует. Придумайте другой!';
                    $status = 'error';
                    $this->flashMessenger()
                        ->setNamespace($status)
                        ->addMessage($message);
                    return $this->redirect()->toRoute('reg'); 
                }
                
                if ($request->getPost('password')!=$request->getPost('passwordRepeat'))
                {
                    $message= 'Неверно повторен пароль!';
                    $status = 'error';
                    $this->flashMessenger()
                        ->setNamespace($status)
                        ->addMessage($message);
                    return $this->redirect()->toRoute('reg'); 
                }
                 
                $user->setName($request->getPost('name'));
                $user->setPassword($request->getPost('password'));
                $user->setLogin($request->getPost('login'));
                $em->persist($user);
                $em->flush();
            }
            else{
                $message= 'Неккоректные данные!';
                $status = 'error';
                $this->flashMessenger()
                    ->setNamespace($status)
                    ->addMessage($message);
                return $this->redirect()->toRoute('reg'); 
            }
            
              $message= 'Вы успешно зарегистрировались! Теперь вы можете войти в систему';
                $status = 'success';
                $this->flashMessenger()
                    ->setNamespace($status)
                    ->addMessage($message);
                return $this->redirect()->toRoute('login'); 
        }
    }
}
