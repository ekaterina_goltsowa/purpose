<?php 
namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Form\Annotation\AnnotationBuilder;
use Main\Entity\Category;
use Main\Entity\User;
use Main\Entity\Purpose;

class IndexController extends AbstractActionController
{
    public $login;
    
    public function getLogin()
    {
        return $this->getServiceLocator()->get('AuthService')->getStorage()->read();
    }
    
    public function getIdByLogin()
    {
        $login = $this->getLogin();
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT u.id FROM Main\Entity\User u WHERE u.login='$login'");
        $rows = $query->getResult();
        $id=$rows[0]['id'];
        return $id;
    }
    
        public function getNameByLogin()
    {
        $login = $this->getLogin();
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT u.name FROM Main\Entity\User u WHERE u.login='$login'");
        $rows = $query->getResult();
        $name=$rows[0]['name'];
        return $name;
    }
       
    public function getEntityManager()
    {
       return $entityManager=$this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('login');
        }
        
        $login = $this->getLogin();
        $idUser = $this->getIdByLogin();
        $name = $this->getNameByLogin();
        
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT u FROM Main\Entity\Category u WHERE u.user='$idUser' ORDER BY u.id ASC");
        $row = $query->getResult();
        $category = array();
        
        foreach ($row as $item){
            $cat=array();
            $cat['id'] = $item->getId();
            $cat['name'] = $item->getName();
            $cat['path'] = $item->getPath();
            
            $query = $em->createQuery("SELECT u FROM Main\Entity\Purpose u WHERE u.category='".$cat["id"]."'");
            $row_purpose = $query->getResult();
            
            $purposes = array();
            foreach ($row_purpose as $purpose)
            {   $one_purpose=array();
                $one_purpose['id'] = $purpose->getId();
                $one_purpose['name'] = $purpose->getName();
                $one_purpose['isDone'] = $purpose->getIsDone();
                $purposes[] = $one_purpose;
            }
            
            $cat['purpose'] = $purposes;
            $category[]=$cat;           
        }
        
        $query = $em->createQuery("SELECT u FROM Main\Entity\Icon u ");
        $icons = $query->getResult();
  
        return array('purposes'=>$category, 'name'=>$name, 'icons'=>$icons);
    }
    
    public function checkIsCategory($name)
    {
        $em= $this->getEntityManager();
        $login = $this->getIdByLogin();
        $query = $em->createQuery("SELECT u.id FROM Main\Entity\Category u WHERE u.name='$name' and u.user='$login'" );
        $rows = $query->getResult();
        
       if (count($rows)>0)
            return true;
        else return false;
    }
    

    public function addCategoryAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost();
         $response = $this->getResponse();
        
        if ($this->checkIsCategory($data['name']))
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'error')));
        
        $em= $this->getEntityManager();
        $category = new Category();
        $category->setName($data['name']);
        
        $repository = $em->getRepository('Main\Entity\User');
        $user = $repository->find($this->getIdByLogin());
        $category->setUser($user);
        
        $repository = $em->getRepository('Main\Entity\Icon');
        $icon = $repository->find($data['path']);
        $category->setPath($icon);
        
        $em->persist($category);
        $em->flush();
        
        return $response->setContent(\Zend\Json\Json::encode(array('id'=>$category->getId(), 'path'=>$data['path'], 'result'=>'success')));
    }
    
    
    public function editCategoryAction()
    {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse();
        
        
            $category=$em->find('Main\Entity\Category', $data['id']);
            $category->setName($data['name']);  
            
            $repository = $em->getRepository('Main\Entity\Icon');
            $icon = $repository->find($data['path']);
            $category->setPath($icon);
            
            $em->persist($category);
            $em->flush();
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success')));
        
    }

    public function deleteCategoryAction()
      {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse(); 
        try{
            $repository = $em->getRepository('Main\Entity\Category');
            $category = $repository->find($data['id']);
            $em->remove($category);
            $em->flush();
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success')));    
        }
        catch (\Exception $ex) {
           return $response->setContent(\Zend\Json\Json::encode(array('result'=>'error')));   
        }
     }
     public function addPurposeAction()
     {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse(); 
        
        try{
            $purpose = new Purpose();
            $purpose->setName($data['name']);
            
            $repository = $em->getRepository('Main\Entity\User');
            $user = $repository->find($this->getIdByLogin());
            $purpose->setUser($user);
            
            $repository = $em->getRepository('Main\Entity\Category');
            $category= $repository->find($data['id']);
            $purpose->setCategory($category);
            
            $purpose->setIsdone(0);
            
            $em->persist($purpose);
            $em->flush();
        
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success','id'=>$purpose->getId())));    
       }
        catch (\Exception $ex) {
           return $response->setContent(\Zend\Json\Json::encode(array('result'=>'error')));   
        }
     }
     
     public function editPurposeAction()
    {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse();
        
        try {
            $purpose=$em->find('Main\Entity\Purpose', $data['id']);
            $purpose->setName($data['name']);  
            $em->persist($purpose);
            $em->flush();
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success')));
        }
        catch (\Exception $ex){
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'error')));
        }
    }
     
      public function deletePurposeAction()
      {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse(); 
        try{
            $repository = $em->getRepository('Main\Entity\Purpose');
            $purpose = $repository->find($data['id']);
            $em->remove($purpose);
            $em->flush();
            return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success')));    
        }
        catch (\Exception $ex) {
           return $response->setContent(\Zend\Json\Json::encode(array('result'=>'error')));   
        }
     } 
     
     public function editDoneAction()
     {
        $em= $this->getEntityManager();
        $request = $this->getRequest();
        $data = $request->getPost();
        $response = $this->getResponse();
        
        $purpose=$em->find('Main\Entity\Purpose', $data['id']);
        $purpose->setIsdone($data['isDone']);  
        $em->persist($purpose);
        $em->flush();  
        
        return $response->setContent(\Zend\Json\Json::encode(array('result'=>'success')));
     }
}