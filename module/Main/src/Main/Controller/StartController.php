<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Form\Annotation\AnnotationBuilder;
use Main\Entity\Category;
use Main\Entity\User;
use Main\Entity\Purpose;

class StartController extends AbstractActionController
{

    public function indexAction() {

        return new ViewModel();
    }
}
