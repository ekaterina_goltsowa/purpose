<?php
namespace Main\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Main\Entity\User;
use Main\Filter\LoginFormInputFilter;

class LoginForm extends Form implements ObjectManagerAwareInterface
{
    protected $objectManager;
      
      public function setObjectManager(ObjectManager $objectManager)
      {
           $this->objectManager = $objectManager;
      }
      
      public function getObjectManager()
      {
          return $this->objectManager;
      }
      
      public function __construct(ObjectManager $objectManager) {
          parent::__construct('loginForm');
          $this ->setObjectManager($objectManager);
          $this->createElements();
      }
      
      public function createElements()
      {
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'bs-example form-horizontal');
        
       $this->setInputFilter(new LoginFormInputFilter());
        
         
        $this->add(array(
            'name'=>'login',
            'type'=>'Text',
            'options'=>array(
                'min'=>3,
                'max'=>25,
                'label'=>'Введите логин',               
            ),
            'attributes'=>array(
                'class'=>'form-control',
                'required'=>'required',
            ),
        ));
                  
        $this->add(array(
            'name'=>'password',
            'type'=>'Password',
            'options'=>array(
                'min'=>6,
                'max'=>20,
                'label'=>'Введите пароль',               
            ),
            'attributes'=>array(
                'class'=>'form-control',
                'required'=>'required',
            ),
        ));


                
        $this->add(array(
            'name'=>'submit',
            'type'=>'Submit',
            'attributes'=>array(
                'class'=>'btn btn-primary',
                'value' => 'Войти',
                'class'=>'btn btn-ptimary'
            ),
        )); 
      }
}