<?php

namespace Main\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Purpose
 *
 * @ORM\Table(name="purpose", indexes={@ORM\Index(name="category_index", columns={"category"}), @ORM\Index(name="user", columns={"user"})})
 * @ORM\Entity
 */
class Purpose
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDone", type="boolean", nullable=false)
     */
    private $isdone;

    /** 
     * @var \Main\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="Main\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category", referencedColumnName="id")
     * })
     * @ORM\JoinColumn(name="category", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var \Main\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Main\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Purpose
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isdone
     *
     * @param boolean $isdone
     *
     * @return Purpose
     */
    public function setIsdone($isdone)
    {
        $this->isdone = $isdone;

        return $this;
    }

    /**
     * Get isdone
     *
     * @return boolean
     */
    public function getIsdone()
    {
        return $this->isdone;
    }

    /**
     * Set category
     *
     * @param \Main\Entity\Category $category
     *
     * @return Purpose
     */
    public function setCategory(\Main\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Main\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set user
     *
     * @param \Main\Entity\User $user
     *
     * @return Purpose
     */
    public function setUser(\Main\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Main\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
