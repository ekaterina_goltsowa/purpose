<?php

namespace Main\Entity;

use Zend\Form\Annotation;
use Doctrine\ORM\Mapping as ORM;


/**
 * Category
 *
 * @ORM\Table(name="category", indexes={@ORM\Index(name="user_index", columns={"user"})})
 * @ORM\Entity
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Category")
 */
class Category
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Annotation\Type("Zend\Form\Element\Hidden")
     */
    private $id;

    /** 
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Options({"label":"Введите название категории:"})
     */
    private $name;

    /** 
     * @var \Main\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Main\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     * @Annotation\Type("Zend\Form\Element\Hidden")
     */
    private $user;

    /** 
     * @var \Main\Entity\Icon
     *
     * @ORM\ManyToOne(targetEntity="Main\Entity\Icon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="path", referencedColumnName="id")
     * })
     * @Annotation\Type("Zend\Form\Element\Hidden")
     */
    private $path;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \Main\Entity\User $user
     *
     * @return Category
     */
    public function setUser(\Main\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Main\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
     /**
     * Set path
     *
     * @param \Main\Entity\Icon $path
     *
     * @return Category
     */
    public function setPath(\Main\Entity\Icon $path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return \Main\Entity\Path
     */
    public function getPath()
    {
        return $this->path;
    }
}
