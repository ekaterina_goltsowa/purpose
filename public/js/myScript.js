
    
    function delete_message()
    {
        $('div.alert').hide('slow').remove();
    }
    
    function open_window_delete_account(){
        
        $('#delete-acc').on('shown.bs.modal', function () {
            $('#btn_delete_acc').focus();
        });
        
        $('#btn_delete_acc').attr('OnClick', "delete_account()");
        $('.bs-delete-acc-modal-sm').modal();
    }
    
    function delete_account()
    {  
           location.replace ('/auth/delete-account/');
           
    }
    
    function open_window_remove_category(id){
        
        $('#delete-modal').on('shown.bs.modal', function () {
            $('#btn_save_delete').focus();
        });
        
        $('#btn_save_delete').attr('OnClick', "remove_category('"+id+"')");
        $('.bs-delete-modal-sm').modal();
    }
    
    function remove_category(id)
    {
        var request  = $.ajax({
            url: 'delete-category/',
            type: 'post',
            dataType: 'json',
            data: {id: id}
        });
        
    request.done(function(data) {
        if (data['result']=='success')
        {
            $('div#'+id+'.panel').remove();
            var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Категория успешно удалена.\n\
                        </div>';
            $('#message').append(message); 
            $("body").animate({"scrollTop":0},"slow");
            setTimeout(delete_message, 3000);
        }
        else
        {
            var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Категория не может быть удалена.\n\
                        </div>';
            $('#message').append(message);
            $("body").animate({"scrollTop":0},"slow");
            setTimeout(delete_message, 3000);
        }
    });
        
    }
    
    function open_window_edit_category(id)
    {
        $('button img').parent().removeClass('icon_category_select');
        $('#add-modal').on('shown.bs.modal', function () {
            $('div.modal-body input').focus();
        }); 
        $('.bs-modal-sm .modal-dialog .modal-content .modal-header .modal-title').html('Редактировать');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:first').html('Введите название категории');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:last').show();
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body div:first').show();
        $('.bs-modal-sm .modal-dialog').removeClass('modal-sm');
        $('.bs-modal-sm .modal-dialog').addClass('modal-lg');
        
        if ($('div#'+id+' .panel-heading .panel-title a img').length>0)
        {
            $('input#check_category').val($('div#'+id+' .panel-heading .panel-title a img ~ span').html());
            var path = $('div#'+id+' .panel-heading .panel-title a img').attr('src');
            $('button img[src="'+path+'"]').parent().addClass('icon_category_select');
        }
        else
        {
           $('input#check_category').val($('div#'+id+' .panel-heading .panel-title a span').html());

        }
    
        
        $('input#check_category').focus();
        $('#btn_save_edit_or_add').attr('OnClick', "edit_category('"+id+"')");
        $('.bs-modal-sm').modal();
    }
    
    function edit_category(id)
    {
      if ( $("button").is(".icon_category_select"))
        var path = $('button.icon_category_select').attr('id').slice(5);
      else 
        var path = null;
    
      var name =$('input#check_category').val();
      if (name=='')
      {
          var content = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Название категории не может быть пустым.\n\
                        </div>';
          $('#message').append(content);  
          $("body").animate({"scrollTop":0},"slow");
          setTimeout(delete_message, 3000);
          return;
      }     
      var request  = $.ajax({
            url: 'edit-category/',
            type: 'post',
            dataType: 'json',
            data: {name: name, id: id, path:path}
        });
        request.done(function(data) {
            if (data['result']=='success')               
            { 
                $('div#'+id+' .panel-heading .panel-title a').html('<span>'+name+'</span>');
                var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Категория успешно отредактировано.\n\
                        </div>';
            }
            else {
                var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Такая категория уже существует. \n\
                        </div>';
            }
            
            if ( $("button").is(".icon_category_select")) 
            
                $('div#'+id+'.panel h4 a').prepend('<img src="'+$('button.icon_category_select img').attr('src')+'" alt="" width="50px" height="50px">');
            
            
            $('#message').append(message); 
            $("body").animate({"scrollTop":0},"slow");
            setTimeout(delete_message, 3000);
        });
        
    }
    
    function open_window_remove_purpose(id, id_category){
        
        $('#delete-modal').on('shown.bs.modal', function () {
            $('#btn_save_delete').focus();
        });
        
        $('#btn_save_delete').attr('OnClick', "remove_purpose('"+id+"','"+id_category+"')");
        $('.bs-delete-modal-sm').modal();
    }
    
    function remove_purpose(id, id_category)
    {
         var request  = $.ajax({
            url: 'delete-purpose/',
            type: 'post',
            dataType: 'json',
            data: {id: id}
        });
        
    request.done(function(data) {
        if (data['result']=='success')
        {
            $('li#'+id).remove();
            var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Цель успешно удалена.\n\
                        </div>';
            $('#message').append(message); 
            $("body").animate({"scrollTop":0},"slow");
            change_progress(id_category);
            setTimeout(delete_message, 3000);
        }
        else
        {
            var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Цель не может быть удалена.\n\
                        </div>';
            $('#message').append(message);
            $("body").animate({"scrollTop":0},"slow");

            setTimeout(delete_message, 3000);
        }
    });
    }
    
    function open_window_edit_purpose(id, name)
    {
        $('#add-modal').on('shown.bs.modal', function () {
            $('div.modal-body input').focus();
        });
        
        $('.bs-modal-sm .modal-dialog .modal-content .modal-header .modal-title').html('Редактировать');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:first').html('Введите имя цели');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:last').hide();
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body div:first').hide();
        $('.bs-modal-sm .modal-dialog').removeClass('modal-lg');
        $('.bs-modal-sm .modal-dialog').addClass('modal-sm');
        
        $('input#check_category').val(name);
        $('input#check_category').focus();
        $('#btn_save_edit_or_add').attr('OnClick', "edit_purpose('"+id+"')");
        $('.bs-modal-sm').modal();
    }
    
    function edit_purpose(id)
    {
      var name =$('input#check_category').val();
      if (name=='')
      {
          var content = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Название цели не может быть пустым.\n\
                        </div>';
          $('#message').append(content);  
          $("body").animate({"scrollTop":0},"slow");
          setTimeout(delete_message, 3000);
          return;
      }     
      var request  = $.ajax({
            url: 'edit-purpose/',
            type: 'post',
            dataType: 'json',
            data: {name: name, id: id}
        });
        request.done(function(data) {
            if (data['result']=='success')               
            {      
                $('li#'+id+' span:last').html($('input#check_category').val());
                var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Название цели успешно отредактировано.\n\
                        </div>';
            }
            else {
                var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка!</strong>Навзание цели не может быть отредактировано.\n\
                        </div>';
            }
            $('#message').append(message); 
            $("body").animate({"scrollTop":0},"slow");
            setTimeout(delete_message, 3000);
        });
        
    }
    
    function open_window_add_purpose(id)
    {
        $('#add-modal').on('shown.bs.modal', function () {
            $('div.modal-body input').focus();
        });
        
        $('.bs-modal-sm .modal-dialog .modal-content .modal-header .modal-title').html('Добавить цель');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:first').html('Введите имя цели');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:last').hide();
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body div:first').hide();
        $('.bs-modal-sm .modal-dialog').removeClass('modal-lg');
        $('.bs-modal-sm .modal-dialog').addClass('modal-sm');
        
        $('input#check_category').val('');
        $('#btn_save_edit_or_add').attr('OnClick', "add_purpose('"+id+"')");
        $('.bs-modal-sm').modal();
    }
    
    function add_purpose(id)
    {
      var name = $('input#check_category').val();    
      if (name=='')
      {
          var content = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Название цели не может быть пустым.\n\
                        </div>';
          $('#message').append(content);  
          $("body").animate({"scrollTop":0},"slow");
          setTimeout(delete_message, 3000);
          return;
      }     
      var request  = $.ajax({
            url: 'add-purpose/',
            type: 'post',
            dataType: 'json',
            data: {name: name, id: id}
        });
    request.done(function(data) {
        if (data['result']=='success')
        {                
        var content = '<li class="list-group-item" id="'+data['id']+'">\n\
                         <input type="checkbox" value="" onclick="change_checkbox('+data['id']+','+ id+')"> \n\
                         <div id="icon_for_item">\n\
                             <button class="btn" onclick="open_window_edit_purpose('+"'"+data['id']+"'"+','+"'"+name+"'"+')" > <span class="glyphicon glyphicon-pencil" title="Редактировать цель"> </button> \n\
                             <button class="btn" onclick="open_window_remove_purpose('+"'"+data['id']+"'"+','+"'"+id+"'"+')" > </span> <span class="glyphicon glyphicon-remove" title="Удалить цель"></span> </button>\n\
                             </div> <span>'+name+' </span>\n\
                        </li>';
                 
        $('div#'+id+' .panel-collapse .panel-body .list-group').append(content); 
        change_progress(id);
        
        var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Цель успешно добавлена.\n\
                        </div>';  
        }
        else
        {
        var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Цель не может быть добавлена!\n\
                        </div>';
        }
        $('#message').append(message);  
        $("body").animate({"scrollTop":0},"slow");
        setTimeout(delete_message, 3000);
    });    
    }
    
    function open_window_add_category()
    {
        $('button.icon_category_select').removeClass('icon_category_select');
        $('#add-modal').on('shown.bs.modal', function () {
            $('div.modal-body input').focus();
        });
        
        $('.bs-modal-sm .modal-dialog .modal-content .modal-header .modal-title').html('Добавить');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:first').html('Введите название категории');
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body label:last').show();
        $('.bs-modal-sm .modal-dialog .modal-content .modal-body div:first').show();
        $('.bs-modal-sm .modal-dialog').removeClass('modal-sm');
        $('.bs-modal-sm .modal-dialog').addClass('modal-lg');
        
        $('input#check_category').val('');
        $('#btn_save_edit_or_add').attr('OnClick', "add_category()");
        $('.bs-modal-sm').modal();
        
    }
    
    function add_category()
    {
      var name = $('input#check_category').val();
      if ( $("button").is(".icon_category_select"))
        var path = $('button.icon_category_select').attr('id').slice(5);
      else 
        var path = null;
      
      if (name=='')
      {
          var content = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Название категории не может быть пустым.\n\
                        </div>';
          $('#message').append(content);  
          $("body").animate({"scrollTop":0},"slow");
          setTimeout(delete_message, 3000);
          return;
      }
      
      var request  = $.ajax({
            url: 'add-category/',
            type: 'post',
            dataType: 'json',
            data: {name: name, path:path}
        });
        
    request.done(function(data) {
        
       if (data['result']=='success')
        {
        
        var name = $('input#check_category').val();
        var id_panel = 'panel_'+$('div.panel').length;
        var content = '<div class="panel panel-default col-lg-3 pull-left" id="'+data['id']+'">\n\
                         <div class="panel-heading">\n\
                            <h4 class="panel-title">\n\
                                <a data-toggle="collapse" data-toggle="#accordion" href="#'+id_panel+'"> <span>'+name+' </span></a>\n\
                         <div id="icon_for_item">\n\
                            <button class="btn btn-success" onclick="open_window_add_purpose('+"'"+data['id']+"'"+')" ><span class="glyphicon glyphicon-plus" title="Добавить цель"></span></button>\n\
                            <button class="btn btn-primary" onclick="open_window_edit_category('+"'"+data['id']+"'"+')"><span class="glyphicon glyphicon-pencil" title="Редактировать категорию"></span></button>'         
                            +'<button class="btn btn-danger" onclick="open_window_remove_category('+"'"+data['id']+"'"+')" ><span class="glyphicon glyphicon-remove" title="Удалить категорию"></span></button>\n\
                         </div>\n\
                            </h4>\n\
                        </div>\n\
            <div id="'+id_panel+'" class="panel-collapse collapse in">\n\
             <div class="panel-body">\n\
                 <ul class="list-group">'
                 +'</ul>\n\
             </div> </div>\n\
        <div class="progress progress-striped">\n\
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">\n\
                 <span></span>\n\
            </div>\n\
        </div>\n\
  </div>';
         var message = '<div class="alert alert-success alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Отлично! </strong>Категория успешно добавлена.\n\
                        </div>';
      
            $('#accordion').append(content);
            
            if ( $("button").is(".icon_category_select"))
                $('div#'+data['id']+'.panel h4 a').prepend('<img src="'+$('button.icon_category_select img').attr('src')+'" alt="" width="50px" height="50px">');
            
        }
    else {
         var message = '<div class="alert alert-danger alert-dismissable">\n\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                            <strong>Ошибка! </strong>Категория уже существует! Введите другое название. \n\
                        </div>';
    }
        $('#message').append(message);           
        $("body").animate({"scrollTop":0},"slow");
        setTimeout(delete_message, 3000);
        
    });
    
    }
    
    function change_progress(id_category)
    {
        var isDone = $('div#'+id_category+' div.collapse div.panel-body ul.list-group li input:checked').length;
        var allCount = $('div#'+id_category+' div.collapse div.panel-body ul.list-group li input').length;
       
        if(allCount!=0)
            var percent = Math.floor((isDone/allCount)*100);
        else
            var percent=0;
        
        $('div#'+id_category+' div.progress div.progress-bar').css('width',percent+'%');
        $('div#'+id_category+' div.progress div.progress-bar').attr('aria-valuenow',percent);
        
        if (percent>15)
        {
            $('div#'+id_category+' div.progress div.progress-bar span').html(percent+'%');
        }
        else 
        {
            $('div#'+id_category+' div.progress div.progress-bar span').html('');
        }
        
        if (percent==100)
            return true;
        else false;
    }
    
    function change_checkbox(id_purpose, id_category)
    {
        if ($('li#'+id_purpose+' input').prop('checked'))
        {   
            var request  = $.ajax({
            url: 'edit-done/',
            type: 'post',
            dataType: 'json',
            data: {id: id_purpose, isDone: 1}
        });
        if (change_progress(id_category))
        {
            var r=3+parseInt(Math.random()*3);
            for(var i=r; i--;)
            {
                createFirework(31,153,6,null,null,null,null,null,Math.random()>0.5,true);
            }  
            $('div#'+id_category+' div.progress div.progress-bar').removeClass('progress-bar-warning');
            $('div#'+id_category+' div.progress div.progress-bar').addClass('progress-bar-success');
        }
        else 
            createFirework(31,153,6,7,null,null,null,null,false,true);
        }
        else
        {
            var request  = $.ajax({
            url: 'edit-done/',
            type: 'post',
            dataType: 'json',
            data: {id: id_purpose, isDone: 0}
        });
        change_progress(id_category);
        if ( $('div#'+id_category+' div.progress div.progress-bar.progress-bar-success').length>0)
        {
            $('div#'+id_category+' div.progress div.progress-bar').removeClass('progress-bar-success');
            $('div#'+id_category+' div.progress div.progress-bar').addClass('progress-bar-warning');
            
        }

        }                       
}

function select_icon_category(id_icon)
{
    id_icon = 'icon_'+id_icon;

    $('button.icon_category_select').removeClass('icon_category_select');
    $('button#'+id_icon).addClass('icon_category_select');

    
  
}


